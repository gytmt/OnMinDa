package com.newthread.android.mail;

/**
 * Created by 翌日黄昏 on 13-11-20.
 */
public class MailAuthor {
    private String address;
    private String from;
    private String password;

    public MailAuthor() {
        this.address = "";
        this.from = "";
        this.password = "";
    }

    public MailAuthor(String address, String password) {
        this.address = address;
        this.from = address;
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
