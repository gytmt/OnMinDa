package com.newthread.android.client;

import android.content.Context;
import cn.bmob.v3.listener.DeleteListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;
import com.newthread.android.bean.remoteBean.bbs.PostType;
import com.newthread.android.manager.iMangerService.IRemoteService;

/**
 * Created by jindongping on 15/6/1.
 */
public class PostTypeClient implements IRemoteService<PostType> {
    private Context context;

    public PostTypeClient(Context context) {
        this.context = context;
    }

    @Override
    public void add(PostType postType, SaveListener saveListener) {
        postType.save(context, saveListener);
    }

    @Override
    public void update(PostType postType, UpdateListener updateListener) {
        postType.update(context, updateListener);
    }

    @Override
    public void querry(PostType postType, FindListener<PostType> findListener) {

    }

    @Override
    public void delete(PostType postType, DeleteListener deleteListener) {

    }
}
