
package com.newthread.android.bean.bbs;

public class ChatMsgEntity {
    private static final String TAG = ChatMsgEntity.class.getSimpleName();

    private String name;//消息显示出来人的名字

    private String headPhoto;//人都头像

    private String date;//日期

    private String text;//类容

    private String time;//如果是语音，可以设置持续时间单位是秒


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    private boolean isComMeg = true;//显示左边(false)还是右边(true)

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getMsgType() {
        return isComMeg;
    }

    /**
     * false表示信息是你自己发的
     *
     * @param isComMsg 信息来源
     */
    public void setMsgType(boolean isComMsg) {
        isComMeg = isComMsg;
    }

    public String getHeadPhoto() {
        return headPhoto;
    }

    public void setHeadPhoto(String headPhoto) {
        this.headPhoto = headPhoto;
    }

    public ChatMsgEntity() {
    }

    public ChatMsgEntity(String name, String date, String text, boolean isComMsg, String headPhoto) {
        super();
        this.name = name;
        this.date = date;
        this.text = text;
        this.isComMeg = isComMsg;
        this.headPhoto = headPhoto;
    }

    @Override
    public String toString() {
        return "ChatMsgEntity{" +
                "name='" + name + '\'' +
                ", headPhoto='" + headPhoto + '\'' +
                ", date='" + date + '\'' +
                ", text='" + text + '\'' +
                ", time='" + time + '\'' +
                ", isComMeg=" + isComMeg +
                '}';
    }
}
