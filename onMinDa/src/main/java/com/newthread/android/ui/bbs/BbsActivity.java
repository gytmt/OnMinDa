package com.newthread.android.ui.bbs;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import cn.jpush.im.android.eventbus.EventBus;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.newthread.android.R;
import com.newthread.android.activity.main.BaseSFActivity;
import com.newthread.android.activity.main.MyAccountActivity;
import com.newthread.android.adapter.BbsMainVPAdapter;
import com.newthread.android.util.MyPreferenceManager;
import com.newthread.android.view.JazzyViewPager;
import com.viewpagerindicator.IcsLinearLayout;
import com.viewpagerindicator.TabPageIndicator;

/**
 * Created by jindongping on 15/6/4.
 */
public class BbsActivity extends BaseSFActivity {

    //    @ViewInject(id = R.id.viewpager_bbs_main)
    private JazzyViewPager viewPager;
    private TextView tv1, tv2, tv3;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bbs_main);
//        FinalActivity.initInjectedView(this);
        viewPager = (JazzyViewPager) findViewById(R.id.viewpager_bbs_main);
        getSupportActionBar().setTitle("民大交流");
        setViewPager();
        MyPreferenceManager.init(getApplicationContext());
        if (MyPreferenceManager.getString("admin_system_account", "").length() < 1) {
            Intent _intent = new Intent(getApplicationContext(), MyAccountActivity.class);
            startActivity(_intent);
            Toast.makeText(getApplicationContext(), "请先登录教务系统", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        viewPager = null;
        tv1 = null;
        tv2 = null;
        tv3 = null;
        System.gc();
        super.onDestroy();
    }

    /**
     * 设置viewPager效果
     */
    private void setViewPager() {
        FragmentPagerAdapter adapter = new BbsMainVPAdapter(getSupportFragmentManager(), viewPager);
        viewPager.setTransitionEffect(JazzyViewPager.TransitionEffect.ZoomIn);
        viewPager.setPageMargin(30);
        viewPager.setAdapter(adapter);
        final TabPageIndicator indicator = (TabPageIndicator) findViewById(R.id.tabPageIndicator_bbs_main);
        indicator.setViewPager(viewPager);
        IcsLinearLayout icsLinearLayout = (IcsLinearLayout) indicator.getChildAt(0);
        //得到那3个东西 逼着我破坏他的封装性，没办法
        tv1 = (TextView) icsLinearLayout.getChildAt(0);
        tv2 = (TextView) icsLinearLayout.getChildAt(1);
        tv3 = (TextView) icsLinearLayout.getChildAt(2);
        tv1.setBackgroundColor(getResources().getColor(R.color.white));
        tv2.setBackgroundColor(getResources().getColor(R.color.white));
        tv3.setBackgroundColor(getResources().getColor(R.color.white));
        setCurrentTV(0);
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                setCurrentTV(position);
                setMenuState(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indicator.mTabClickListener.onClick(v);
                setCurrentTV(0);
            }
        });
        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indicator.mTabClickListener.onClick(v);
                setCurrentTV(1);
            }
        });
        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indicator.mTabClickListener.onClick(v);
                setCurrentTV(2);
            }
        });

    }

    private void setMenuState(int position) {
        if (menu == null)
            return;
        if (position == 0) {
            menu.clear();
        }
        if (position == 1) {
            menu.clear();
            menu.add("分组").setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }
        if (position == 2) {
            menu.clear();
            menu.add("发帖").setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
            menu.add("我的收藏");
            menu.add("我的回复");
            menu.add("过滤");
        }
    }


    private void setCurrentTV(int index) {
        if (index == 0) {
            tv1.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.skin_tab_icon_conversation_selected, 0, 0);
            tv2.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.skin_tab_icon_contact_normal, 0, 0);
            tv3.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.skin_tab_icon_plugin_normal, 0, 0);
        } else if (index == 1) {
            tv1.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.skin_tab_icon_conversation_normal, 0, 0);
            tv2.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.skin_tab_icon_contact_selected, 0, 0);
            tv3.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.skin_tab_icon_plugin_normal, 0, 0);
        } else if (index == 2) {
            tv1.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.skin_tab_icon_conversation_normal, 0, 0);
            tv2.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.skin_tab_icon_contact_normal, 0, 0);
            tv3.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.skin_tab_icon_plugin_selected, 0, 0);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        //必须要这样操作下否则不显示 Menu
        final MenuItem menuItem = menu.add("");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                menuItem.setVisible(false);
            }
        }, 50);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
        }
        if (item.getTitle().equals("分组")) {
            EventBus.getDefault().post(BbsGroupFrament.STATES.展开分组菜单);
        }
        if (item.getTitle().equals("发帖")) {
            EventBus.getDefault().post(BbsHotFrament.STATES.发帖);
        }
        if (item.getTitle().equals("我的收藏")) {
            EventBus.getDefault().post(BbsHotFrament.STATES.我的收藏);
        }
        if (item.getTitle().equals("我的回复")) {
            EventBus.getDefault().post(BbsHotFrament.STATES.我的回复);
        }
        if (item.getTitle().equals("过滤")) {
            EventBus.getDefault().post(BbsHotFrament.STATES.过滤);
        }
        return super.onMenuItemSelected(featureId, item);

    }
}
