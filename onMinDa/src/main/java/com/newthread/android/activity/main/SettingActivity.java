package com.newthread.android.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.newthread.android.R;
import com.newthread.android.ui.fangDao.FangDaoActivity;
import com.newthread.android.util.AppManager;
import com.newthread.android.util.UpdateManager;

public class SettingActivity extends SherlockActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);

		initView();
	}

	// 初始化界面
	private void initView() {
		ActionBar ab = getSupportActionBar();
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(false);
		ab.setHomeButtonEnabled(true);
		ab.setTitle("设置");

		// 获取当前版本号
		try {
			((TextView) this.findViewById(R.id.current_ver)).setText("当前版本:V"
					+ AppManager.getVersionName(getApplicationContext()));
		} catch (Exception e) {
			e.printStackTrace();
		}

		// 检查更新
		this.findViewById(R.id.check_update).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						// checkAppUpdate();
						// 检查更新
//						new UpdateManager(SettingActivity.this).checkUpdate();
                        new UpdateManager(SettingActivity.this).checkBmobUpdate();
					}
				});

		// 问题反馈
		this.findViewById(R.id.feedback).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						Intent _intent = new Intent(SettingActivity.this,
								FeedbackActivity.class);
						startActivity(_intent);
					}
				});

		// 关于应用
		this.findViewById(R.id.about).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent _intent = new Intent(SettingActivity.this,
						AboutActivity.class);
				startActivity(_intent);
			}
		});
        this.findViewById(R.id.fangdao).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent _intent = new Intent(SettingActivity.this,
                        FangDaoActivity.class);
                startActivity(_intent);
            }
        });
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
